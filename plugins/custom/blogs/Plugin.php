<?php 
namespace Custom\Blogs;

use Backend;
use Controller;
/**
 * The plugin.php file (called the plugin initialization script) defines the plugin information class.
 */


use System\Classes\PluginBase;

class Plugin extends PluginBase
{

    public $elevated = true;

    public function pluginDetails()
    {
        return [
            'name'        => 'October Blog',
            'description' => 'This is Blog Demo',
            'author'      => 'Denish Patel',
            'icon'        => 'icon-leaf'
        ];
    }


    public function registerNavigation()
    {
        return [
            'blogs' => [
                'label'       => 'Blog',
                'url'         => Backend::url('custom/blogs/posts'),
                'icon'        => 'icon-pencil',
                'permissions' => ['custom.blogs.*'],
                'order'       => 500,
            ]
        ];
    }

}
