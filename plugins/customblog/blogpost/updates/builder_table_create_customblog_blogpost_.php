<?php namespace Customblog\Blogpost\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateCustomblogBlogpost extends Migration
{
    public function up()
    {
        Schema::create('customblog_blogpost_', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('id');
            $table->text('title');
            $table->text('summery');
            $table->text('content');
            $table->text('images');
            $table->text('category');
            $table->primary(['id']);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('customblog_blogpost_');
    }
}
