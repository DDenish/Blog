<?php namespace Denish\Blog\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
//use Input;
/**
 * Posts Back-end Controller
 */
class Posts extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Denish.Blog', 'blog', 'posts');
    }

    /*public function create_onSave()
    {
        $inputs = post('Post');

        //echo "<pre>";print_r($inputs);
        //echo "<pre>";print_r(Input::file('blog_image'));exit;
        // save team
        $postModel = new \Denish\blog\Models\Post;
        $postModel->title = $inputs['title'];
        $postModel->slug = $inputs['slug'];
        $postModel->summary = $inputs['summary'];
        $postModel->content = $inputs['content'];
        
        $postModel->blog_image = Input::file('blog_image');
        $postModel->is_published = 1;
        $postModel->save();

        
        \Flash::success("Post saved successfully");
        
        return $this->makeRedirect('update', $postModel);
    }


    public function update_onSave($recordId)
    {
        $inputs = post('Post');

        $this->asExtension('FormController')->update_onSave($recordId);
        \Flash::success("Post updated successfully");
        $postModel = new \Denish\blog\Models\Post;
        return $this->makeRedirect('update', $postModel);
    }

    public function update_onDelete($recordId)
    {
        $teamModel = \Denish\Blog\Models\Post::findOrFail($recordId);
        $teamModel->delete();
        \Flash::success("Post deleted successfully");

        return $this->makeRedirect('delete', $teamModel);
    }*/
}
