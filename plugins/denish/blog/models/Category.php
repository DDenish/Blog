<?php 
namespace Denish\Blog\Models;

use Model;
use Denish\Blog\Models\Post;
/**
 * Category Model
 */
class Category extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'denish_blog_categories';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    
    public $belongsTo = [];
    public $belongsToMany = [
        'posts'=>[
            'Denish\Blog\Models\Post',
            'table' => 'denish_blog_posts_categories',
            'order' => 'created_at desc',
        ]
    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    use \October\Rain\Database\Traits\Validation;

    public $rules = [
        'name' => 'required',
        'slug' => ['required', 'regex:/^[a-z0-9\/\:_\-\*\[\]\+\?\|]*$/i', 'unique:denish_blog_categories']
    ];
}
