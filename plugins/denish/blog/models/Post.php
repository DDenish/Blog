<?php namespace Denish\Blog\Models;

use Model;
use Denish\Blog\Models\Category;
/**
 * Post Model
 */
class Post extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'denish_blog_posts';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    /*public $belongsTo = ['categories' => [
            'Denish\Blog\Models\Category',
            'table' => 'denish_blog_categories',
            'order' => 'name',
            'key'=>'category_id'
        ]];*/
    public $belongsToMany = ['categories' => [
            'Denish\Blog\Models\Category',
            'table' => 'denish_blog_posts_categories',
            'order' => 'name',
        ]];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = ['blog_image' => 'System\Models\File'];

    use \October\Rain\Database\Traits\Validation;

    public $rules = [
        'title' => 'required',
        'slug' => ['required', 'regex:/^[a-z0-9\/\:_\-\*\[\]\+\?\|]*$/i', 'unique:denish_blog_posts'],
        'content'=>'required',
    ];

    public function getCategoryIdOptions()
    {
        $model = Category::lists('name', 'id');
        //echo "<pre>";print_r($model);exit;
        return $model;

    }

    /**
     * Sets the "url" attribute with a URL to this object
     * @param string $pageName
     * @param Cms\Classes\Controller $controller
     */
    public function setUrl($pageName, $controller)
    {
        $params = [
            'id'   => $this->id,
            'slug' => $this->slug,
        ];

        if (array_key_exists('categories', $this->getRelations())) {
            $params['category'] = $this->categories->count() ? $this->categories->first()->slug : null;
        }

        
        return $this->url = $controller->pageUrl($pageName, $params);
    }

    public function scopeListFrontEnd($query, $options)
    {
        /*
         * Default options
         */
        extract(array_merge([
            'page'       => 1,
            'perPage'    => 30,
            'sort'       => 'created_at',
            'categories' => null,
            'category'   => null,
            'search'     => '',
        ], $options));

        return $query->paginate($perPage, $page);
    }
}
