<?php namespace Denish\Blog\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreatePostCategoriesTable extends Migration
{
    public function up()
    {
        Schema::create('denish_post_categories', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('post_id',11);
            $table->integer('category_id',11);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('denish_blog_posts');
    }
}
