<?php namespace Denish\Blog\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreatePostsTable extends Migration
{
    public function up()
    {
        Schema::create('denish_blog_posts', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title',255);
            $table->string('slug')->index();
            $table->text('summary')->nullable();
            $table->integer('category_id')->unsigned()->index();
            $table->foreign('category_id')->references('id')->on('denish_blog_categories');
            $table->text('content');
            $table->boolean('is_published')->default(false);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('denish_blog_posts');
    }
}
