<?php 
use Denish\Blog\models\Post;use Denish\Blog\models\Category;class Cms5b5ec62117703169840604_18460273563024d094472e0d3890beffClass extends Cms\Classes\PageCode
{


public function onStart()
{
   if (!empty($_REQUEST['search'])) {
    
       $input = Input::all();
       $searchval = trim($input['search']);
       $this['searchval']=$searchval;
       $posts = Db::table('denish_blog_posts as p')
                    ->leftJoin('denish_blog_posts_categories as c', 'p.id', '=', 'c.post_id')
                    ->leftJoin('denish_blog_categories as bc', 'c.category_id', '=', 'bc.id')
                    ->where('p.title', 'LIKE', '%' . $searchval . '%')
                    ->orWhere('bc.name', 'LIKE', '%' . $searchval . '%')
                    ->groupBy('p.id')
                    ->select('p.*',DB::raw("group_concat(c.category_id) as cat_ids"),DB::raw("group_concat(bc.name) as cat_name"))
                    ->paginate(1);
       //$posts = Post::where('title','LIKE',"%{$searchval}%")->paginate(1);
       //$posts = Post::with('categories')where('title','LIKE',"%{$searchval}%")->paginate(1);
       //echo "<pre>";print_r($posts);exit;
       if(!empty($posts))
       {
           $posts->appends(['search' => $searchval]);
           $this['posts']=$posts;
       }
       else
       {
           $this['posts']=array();
       }
       //$this['categories'] = Category::all();
    }
    else
    {
       $posts = Db::table('denish_blog_posts as p')
                    ->leftJoin('denish_blog_posts_categories as c', 'p.id', '=', 'c.post_id')
                    ->leftJoin('denish_blog_categories as bc', 'c.category_id', '=', 'bc.id')
                    ->groupBy('p.id')
                    ->select('p.*',DB::raw("group_concat(c.category_id) as cat_ids"),DB::raw("group_concat(bc.name) as cat_name"))
                    ->paginate(1);
      
       //echo "<pre>";print_r($posts);exit;
       if(!empty($posts))
       {
           $this['posts']=$posts;
       }
       else
       {
           $this['posts']=array();
       }
       //$this['posts'] = Post::paginate(1);
       //$this['categories'] = Category::all();
    }
}
}
