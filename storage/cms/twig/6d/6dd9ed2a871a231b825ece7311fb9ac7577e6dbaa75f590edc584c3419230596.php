<?php

/* E:\wamp64\www\blogdemo/themes/demo/pages/Blog.htm */
class __TwigTemplate_0877cc2348d01de6b23154baeefc73354872909742fe16ddbb9066284a157a98 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div>
    <div style=\"float:left; width:70%;\">
    <ul class=\"post-list\">
        ";
        // line 4
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["posts"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["post"]) {
            // line 5
            echo "        <li>
            <h3><b>";
            // line 6
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "title", array()), "html", null, true);
            echo "</b></h3>
            <h4>";
            // line 7
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "cat_name", array()), "html", null, true);
            echo "</h4>
            <p class=\"excerpt\">";
            // line 8
            echo twig_get_attribute($this->env, $this->source, $context["post"], "content", array());
            echo "</p>
            ";
            // line 9
            if ((twig_get_attribute($this->env, $this->source, $context["post"], "summary", array()) != "")) {
                // line 10
                echo "            <p class=\"excerpt\">";
                echo twig_get_attribute($this->env, $this->source, $context["post"], "summary", array());
                echo "</p>
            ";
            }
            // line 12
            echo "        </li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['post'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 14
        echo "        ";
        echo twig_get_attribute($this->env, $this->source, ($context["posts"] ?? null), "render", array());
        echo "
    </ul>
    </div>
    <div style=\"float:left;width:30%;margin-top:15px;\">
    <form name=\"search-form\"  action=\"";
        // line 18
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("Blog");
        echo "\">
        ";
        // line 19
        if ((($context["searchval"] ?? null) != "")) {
            // line 20
            echo "            <input type=\"text\" name=\"search\" value=\"";
            echo twig_escape_filter($this->env, ($context["searchval"] ?? null), "html", null, true);
            echo "\">
        ";
        } else {
            // line 22
            echo "            <input type=\"text\" name=\"search\">
        ";
        }
        // line 24
        echo "    <input type=\"submit\" value=\"Go\">
    </form>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "E:\\wamp64\\www\\blogdemo/themes/demo/pages/Blog.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  86 => 24,  82 => 22,  76 => 20,  74 => 19,  70 => 18,  62 => 14,  55 => 12,  49 => 10,  47 => 9,  43 => 8,  39 => 7,  35 => 6,  32 => 5,  28 => 4,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div>
    <div style=\"float:left; width:70%;\">
    <ul class=\"post-list\">
        {% for  post in posts %}
        <li>
            <h3><b>{{post.title}}</b></h3>
            <h4>{{post.cat_name}}</h4>
            <p class=\"excerpt\">{{post.content |raw}}</p>
            {% if post.summary != '' %}
            <p class=\"excerpt\">{{post.summary |raw}}</p>
            {% endif %}
        </li>
        {% endfor %}
        {{ posts.render|raw }}
    </ul>
    </div>
    <div style=\"float:left;width:30%;margin-top:15px;\">
    <form name=\"search-form\"  action=\"{{ 'Blog'|page }}\">
        {% if searchval != '' %}
            <input type=\"text\" name=\"search\" value=\"{{ searchval }}\">
        {% else %}
            <input type=\"text\" name=\"search\">
        {% endif %}
    <input type=\"submit\" value=\"Go\">
    </form>
    </div>
</div>", "E:\\wamp64\\www\\blogdemo/themes/demo/pages/Blog.htm", "");
    }
}
