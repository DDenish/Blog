<?php

/* E:\wamp64\www\install-master/plugins/pkleindienst/blogsearch/components/searchform/default.htm */
class __TwigTemplate_7e4b9a86ee6d394b949cfcc4d0455463eb466c2bdf42ab92dad3b6ad79b62732 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<form class=\"form-inline\" method=\"GET\" action=\"";
        echo $this->extensions['Cms\Twig\Extension']->pageFilter(twig_get_attribute($this->env, $this->source, ($context["__SELF__"] ?? null), "resultPage", array()));
        echo "\">
    <input type=\"text\" name=\"search\" class=\"form-control\">

    ";
        // line 4
        if (twig_get_attribute($this->env, $this->source, ($context["__SELF__"] ?? null), "categoryFilter", array())) {
            // line 5
            echo "        <select name=\"cat[]\" class=\"form-control\">
        ";
            // line 6
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["__SELF__"] ?? null), "categories", array()));
            foreach ($context['_seq'] as $context["key"] => $context["cat"]) {
                // line 7
                echo "            <option value=\"";
                echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $context["cat"], "html", null, true);
                echo "</option>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['cat'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 9
            echo "        </select>
    ";
        }
        // line 11
        echo "
    <button class=\"btn btn btn-primary\" type=\"submit\">";
        // line 12
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), array("Search"));
        echo "</button>
</form>";
    }

    public function getTemplateName()
    {
        return "E:\\wamp64\\www\\install-master/plugins/pkleindienst/blogsearch/components/searchform/default.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 12,  54 => 11,  50 => 9,  39 => 7,  35 => 6,  32 => 5,  30 => 4,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<form class=\"form-inline\" method=\"GET\" action=\"{{ __SELF__.resultPage|page }}\">
    <input type=\"text\" name=\"search\" class=\"form-control\">

    {% if __SELF__.categoryFilter %}
        <select name=\"cat[]\" class=\"form-control\">
        {% for key,cat in __SELF__.categories %}
            <option value=\"{{ key }}\">{{ cat }}</option>
        {% endfor %}
        </select>
    {% endif %}

    <button class=\"btn btn btn-primary\" type=\"submit\">{{ 'Search'|_ }}</button>
</form>", "E:\\wamp64\\www\\install-master/plugins/pkleindienst/blogsearch/components/searchform/default.htm", "");
    }
}
