<?php

/* E:\wamp64\www\install-master/themes/demo/pages/Blog.htm */
class __TwigTemplate_12beeff6021ed7ded99c3d35e29cce94e939f6cce1cb360fd979f0eab4fb7f72 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div>
    <div style=\"float:left; width:70%;\">
    <ul class=\"post-list\">
        ";
        // line 4
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["posts"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["post"]) {
            // line 5
            echo "        <li>
            <h3><b>";
            // line 6
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "title", array()), "html", null, true);
            echo "</b></h3>
            <h4>";
            // line 7
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "cat_name", array()), "html", null, true);
            echo "</h4>
            <p class=\"excerpt\">";
            // line 8
            echo twig_get_attribute($this->env, $this->source, $context["post"], "content", array());
            echo "</p>
        </li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['post'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 11
        echo "        ";
        echo twig_get_attribute($this->env, $this->source, ($context["posts"] ?? null), "render", array());
        echo "
    </ul>
    </div>
    <div style=\"float:left;width:30%;margin-top:15px;\">
    <form name=\"search-form\"  action=\"";
        // line 15
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("Blog");
        echo "\">
        ";
        // line 16
        if ((($context["searchval"] ?? null) != "")) {
            // line 17
            echo "            <input type=\"text\" name=\"search\" value=\"";
            echo twig_escape_filter($this->env, ($context["searchval"] ?? null), "html", null, true);
            echo "\">
        ";
        } else {
            // line 19
            echo "            <input type=\"text\" name=\"search\">
        ";
        }
        // line 21
        echo "    <input type=\"submit\" value=\"Go\">
    </form>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "E:\\wamp64\\www\\install-master/themes/demo/pages/Blog.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  76 => 21,  72 => 19,  66 => 17,  64 => 16,  60 => 15,  52 => 11,  43 => 8,  39 => 7,  35 => 6,  32 => 5,  28 => 4,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div>
    <div style=\"float:left; width:70%;\">
    <ul class=\"post-list\">
        {% for  post in posts %}
        <li>
            <h3><b>{{post.title}}</b></h3>
            <h4>{{post.cat_name}}</h4>
            <p class=\"excerpt\">{{post.content |raw}}</p>
        </li>
        {% endfor %}
        {{ posts.render|raw }}
    </ul>
    </div>
    <div style=\"float:left;width:30%;margin-top:15px;\">
    <form name=\"search-form\"  action=\"{{ 'Blog'|page }}\">
        {% if searchval != '' %}
            <input type=\"text\" name=\"search\" value=\"{{ searchval }}\">
        {% else %}
            <input type=\"text\" name=\"search\">
        {% endif %}
    <input type=\"submit\" value=\"Go\">
    </form>
    </div>
</div>", "E:\\wamp64\\www\\install-master/themes/demo/pages/Blog.htm", "");
    }
}
